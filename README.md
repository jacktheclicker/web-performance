WebPerf Specifications
==============================

Editors' drafts from the Web Performance Working Group.

Web Performance tests can be found in [web-platforms-tests][WPT]

[WPT]: https://github.com/w3c/web-platform-tests/
